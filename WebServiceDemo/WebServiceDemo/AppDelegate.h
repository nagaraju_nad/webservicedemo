//
//  AppDelegate.h
//  WebServiceDemo
//
//  Created by KANAKARAJU GANDREDDI on 6/26/16.
//  Copyright © 2016 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  SingletoneDataManager.m
//  WebServiceDemo
//
//  Created by KANAKARAJU GANDREDDI on 6/26/16.
//  Copyright © 2016 test. All rights reserved.
//

#import "SingletoneDataManager.h"

@implementation SingletoneDataManager
@synthesize logindict,upcomingDict;
+(instancetype)sharedManager{

    static SingletoneDataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SingletoneDataManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;

    
}
@end

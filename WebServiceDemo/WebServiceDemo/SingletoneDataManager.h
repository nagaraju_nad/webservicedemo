//
//  SingletoneDataManager.h
//  WebServiceDemo
//
//  Created by KANAKARAJU GANDREDDI on 6/26/16.
//  Copyright © 2016 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletoneDataManager : NSObject

@property (strong,nonatomic) NSMutableDictionary *logindict;
@property (strong,nonatomic) NSMutableDictionary *upcomingDict;
+(instancetype)sharedManager;

@end

//
//  UpcomingspotsTableViewController.m
//  WebServiceDemo
//
//  Created by KANAKARAJU GANDREDDI on 7/2/16.
//  Copyright © 2016 test. All rights reserved.
//

#import "UpcomingspotsTableViewController.h"
#import "SingletoneDataManager.h"
#import "UpcommingCell.h"
NSString * const MethodGetImagePrefixPath      = @"http://armalytics.com/RAPS";
@interface UpcomingspotsTableViewController ()
{

    SingletoneDataManager *sharedInstance;
    NSArray *spotsArray;
    NSURLSession *upcomingSession;
    NSMutableData *responseData;

    NSURLSession *sesson;
    NSURLSessionDataTask *dataTask ;
}
@end

@implementation UpcomingspotsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    sharedInstance =  [SingletoneDataManager sharedManager];
    spotsArray = [sharedInstance.upcomingDict objectForKey:@"SpotGridData"];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:@"https://itunes.apple.com/search?term=apple&media=software"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", json);
    }];
   
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return spotsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    static NSString *CellIdentifier = @"UpcommingCell";


    UpcommingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UpcommingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    [self downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",MethodGetImagePrefixPath,[[spotsArray objectAtIndex:indexPath.row] objectForKey:@"Icon"]]] completionBlock:^(BOOL succeeded, UIImage *image) {
        
        if (succeeded) {
            
            cell.imageView.image = image;
        }

    
    }];
    
    
    
    // Configure the cell...
    
    return cell;
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //    NSString *authStr = [NSString stringWithFormat:@"%@:%@", @"cms", @"1234"];
    //    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    //    // NSString *authValue = [authData base64Encoding];
    //    NSString *authValue = [authData base64EncodedStringWithOptions:0];
    //
    //
    //    [request setValue:[NSString stringWithFormat:@"Basic %@",authValue] forHTTPHeaderField:@"Authorization"];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 150;
}

@end

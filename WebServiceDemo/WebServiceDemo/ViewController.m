//
//  ViewController.m
//  WebServiceDemo
//
//  Created by KANAKARAJU GANDREDDI on 6/26/16.
//  Copyright © 2016 test. All rights reserved.
//

#import "ViewController.h"
#import "SingletoneDataManager.h"
#import "UpcomingspotsTableViewController.h"
#define Loginurl = "http://armalytics.com/RAPS/Site/DoLogin";

#define upcomingurl = "http://armalytics.com/LivePulseAPI/GetUpcomingSpotGrid";

#define mappUrl "https://rtss-sit.jioconnect.com:443/MappServer3/service/Servlet"

@interface ViewController ()
{

    NSURLConnection *loginConnection;
    NSURLConnection *mappConnection;

    NSURLSession *upcomingSession;
    NSMutableData *responseData;
    SingletoneDataManager *sharedInstance;
    
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   sharedInstance =  [SingletoneDataManager sharedManager];
   // [self loginAPIService];
    
    [self upcomingAPI];
    //[self mappConnect];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)loginAPIService{

    NSURL *loginUrl = [NSURL URLWithString:@"http://armalytics.com/RAPS/Site/DoLogin"];
    NSMutableURLRequest *resquesr = [NSMutableURLRequest requestWithURL:loginUrl
                               ];
    
    NSMutableDictionary *inputDict = [[NSMutableDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"demo",@"armpurple",@"True",@"ARMALYTICSAPPID", nil] forKeys:[NSArray arrayWithObjects:@"Email",@"Password",@"IsAjaxRequest",@"APPID", nil]];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict options:kNilOptions error:nil];
    [resquesr setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [resquesr setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];

    [resquesr setHTTPBody:jsonData];
    [resquesr setHTTPMethod:@"POST"];
    
    loginConnection = [[NSURLConnection alloc] initWithRequest:resquesr delegate:self];
 
    
}




-(void)mappConnect{
    NSURL *loginUrl = [NSURL URLWithString:@"10.64.77.242/MappServer3/servlet/Service"];
    NSMutableURLRequest *resquesr = [NSMutableURLRequest requestWithURL:loginUrl];
   
    mappConnection = [[NSURLConnection alloc]initWithRequest:resquesr delegate:self];

}

-(void)upcomingAPI{
    
   NSLog(@"Data 11= %@",sharedInstance.upcomingDict);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    //NSURLSession * des = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:[NSOperationQueue currentQueue]];
    
    NSURL * url = [NSURL URLWithString:@"http://armalytics.com/LivePulseAPI/GetUpcomingSpotGrid?ClientID=64&EstimatedImpacts=0&Filter=0"];
//    NSURLSessionDataTask *data =[defaultSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        NSLog(@"Data 22= %@",sharedInstance.upcomingDict);
//    }];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                          //  NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                            sharedInstance.upcomingDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                                            
                                                            NSLog(@"Data33 = %@",sharedInstance.upcomingDict);

                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
{
    responseData= [[NSMutableData alloc]init];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;{

    [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection;{
    NSMutableDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"dictResponse : %@",dictResponse);
    sharedInstance.logindict=dictResponse;
    NSLog(@"dictResponse : %@",sharedInstance.logindict);

}
- (IBAction)dd:(id)sender{

    UpcomingspotsTableViewController *dd = [[UpcomingspotsTableViewController alloc]init];
    [self presentViewController:dd animated:YES completion:nil];

    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    NSLog(@"error :%@",error);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
